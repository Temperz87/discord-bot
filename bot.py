#!python3
import discord
import time
import os
import json
from discord.ext import commands

print('Discord Bot Started, waiting 15 seconds')
time.sleep(15)
print('Finished waiting')

botData = open(os.path.dirname(os.path.abspath(__file__)) + os.sep + "BotData.txt").readlines()
token = 'TOKEN'
welcomeID = 0
serverID = 0
client = commands.Bot(command_prefix='.')
client.remove_command('help')

try:
    open('data.json', 'r')
except FileNotFoundError:
    with open('data.json', 'w') as file:
        toDump = {"mpMentions": 1}
        json.dump(toDump, file)


@client.event
async def on_ready():
    guild = client.get_guild(serverID)
    await client.change_presence(status=discord.Status.online,
                                 activity=discord.Game(str(guild.member_count) + ' members!'))
    print("Bot has logged in")


@client.event
async def on_message(message):
    lowercase = message.content.lower()
    if message.author != client.user:
        if "multiplayer" in lowercase:
            with open('data.json', 'r') as file:
                data = json.load(file)
                data["mpMentions"] = int(data["mpMentions"]) + 1
            with open('data.json', 'w') as file:
                json.dump(data, file)
    await client.process_commands(message)


@client.event
async def on_member_join(member):
    print(f"{member} joined the server")
    channel = client.get_channel(welcomeID)
    guild = client.get_guild(serverID)
    await client.change_presence(status=discord.Status.online,
                                 activity=discord.Game(str(guild.member_count) + ' members!'))
    await channel.send(f'<@{member.id}>  has joined the server!')


@client.event
async def on_member_remove(member):
    print(f"{member} left the server")
    channel = client.get_channel(welcomeID)
    guild = client.get_guild(serverID)
    await channel.send(f'{member}  has left the server')
    await client.change_presence(status=discord.Status.online,
                                 activity=discord.Game(str(guild.member_count) + ' members!'))


@client.command()
async def stats(ctx):
    guild = client.get_guild(serverID)
    await ctx.channel.send(str(
        guild.member_count) + " members\nYou can view more stats here: https://statbot.net/dashboard/597153468834119710")


@client.command()
async def source(ctx):
    await ctx.channel.send("You can view my source code here: https://gitlab.com/vtolvr-mods/discord-bot")


@client.command(aliases=['mpmentions'])
async def mpMentions(ctx):
    with open('data.json', 'r') as file:
        data = json.load(file)
        print(str(data) + " is moetiplayer")
    await ctx.channel.send('The word "Multiplayer" has been said ' + str(data['mpMentions']) + " times.")


@client.command(aliases=['help'])
async def _help(ctx):
    await ctx.channel.send(
        'Available commands\n``.help`` - Displays this message\n``.stats`` - Displays the servers stats\n``.source`` - Posts the URL to the source code of the bot\n``.mpMentions`` - Displays how many times "Multiplayer" has been said')


for line in botData:
    result = line.strip()
    if ("TOKEN=" in result):
        token = result.replace("TOKEN=", "")
    elif ("WELCOME=" in result):
        welcomeID = int(result.replace("WELCOME=", ""))
    elif ("GUILD=" in result):
        serverID = int(result.replace("GUILD=", ""))

client.run(token)
